using System.Reflection;
// <copyright file="UploadTest.cs" company="NodeJsTeam">Copyright © NodeJsTeam 2015</copyright>

using System;
using Microsoft.Pex.Framework;
using Microsoft.Pex.Framework.Validation;
using NUnit.Framework;
using NodeJsClientService;

namespace NodeJsClientService.Tests
{
    /// <summary>This class contains parameterized unit tests for Upload</summary>
    [TestFixture]
    [PexClass(typeof(Upload))]
    [PexAllowedExceptionFromTypeUnderTest(typeof(ArgumentException), AcceptExceptionSubtypes = true)]
    [PexAllowedExceptionFromTypeUnderTest(typeof(InvalidOperationException))]
    public partial class UploadTest
    {

        /// <summary>Test stub for .ctor()</summary>
        [PexMethod]
        public Upload ConstructorTest()
        {
            Upload target = new Upload();
            return target;
            // TODO: add assertions to method UploadTest.ConstructorTest()
        }

        /// <summary>Test stub for TriggerJsonSend(String)</summary>
        [PexMethod(MaxRunsWithoutNewTests = 500)]
        internal bool TriggerJsonSendTest([PexAssumeUnderTest]Upload target, string json)
        {
            bool result = target.TriggerJsonSend(json);
            return result;
            // TODO: add assertions to method UploadTest.TriggerJsonSendTest(Upload, String)
        }

        [PexMethod(MaxRunsWithoutNewTests = 2000)]
        [PexMethodUnderTest("CreateHttpWebRequest()")]
        internal void CreateHttpWebRequest([PexAssumeUnderTest]Upload target)
        {
            object[] args = new object[0];
            Type[] parameterTypes = new Type[0];
            object result = ((MethodBase)(typeof(Upload).GetMethod("CreateHttpWebRequest",
                                                                   BindingFlags.DeclaredOnly | BindingFlags.Instance | BindingFlags.NonPublic, (Binder)null,
                                                                   CallingConventions.HasThis, parameterTypes, (ParameterModifier[])null)))
                                .Invoke((object)target, args);
            // TODO: add assertions to method UploadTest.CreateHttpWebRequest(Upload)
        }
    }
}
