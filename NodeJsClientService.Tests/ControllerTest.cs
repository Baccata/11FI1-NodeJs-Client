// <copyright file="ControllerTest.cs" company="NodeJsTeam">Copyright © NodeJsTeam 2015</copyright>
using System;
using Microsoft.Pex.Framework;
using Microsoft.Pex.Framework.Validation;
using NUnit.Framework;
using NodeJsClientService;

namespace NodeJsClientService.Tests
{
    /// <summary>This class contains parameterized unit tests for Controller</summary>
    [PexClass(typeof(Controller))]
    [PexAllowedExceptionFromTypeUnderTest(typeof(InvalidOperationException))]
    [PexAllowedExceptionFromTypeUnderTest(typeof(ArgumentException), AcceptExceptionSubtypes = true)]
    [TestFixture]
    public partial class ControllerTest
    {
        /// <summary>Test stub for Start()</summary>
        [PexMethod]
        public void StartTest([PexAssumeUnderTest]Controller target)
        {
            target.Start();
            // TODO: add assertions to method ControllerTest.StartTest(Controller)
        }
    }
}
