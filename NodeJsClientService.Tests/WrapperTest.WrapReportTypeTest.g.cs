using Microsoft.ExtendedReflection.DataAccess;
using Microsoft.Pex.Framework.Generated;
using NUnit.Framework;
using Microsoft.Pex.Framework;
using NodeJsClientService;
// <copyright file="WrapperTest.WrapReportTypeTest.g.cs" company="NodeJsTeam">Copyright © NodeJsTeam 2015</copyright>
// <auto-generated>
// This file contains automatically generated tests.
// Do not modify this file manually.
// 
// If the contents of this file becomes outdated, you can delete it.
// For example, if it no longer compiles.
// </auto-generated>
using System;

namespace NodeJsClientService.Tests
{
    public partial class WrapperTest
    {

[Test]
[PexGeneratedBy(typeof(WrapperTest))]
public void WrapReportTypeTest447()
{
    Wrapper wrapper;
    wrapper = new Wrapper();
    wrapper.WrapComputerName();
    wrapper.WrapUserName();
    wrapper.WrapTimestamp(0);
    wrapper.WrapNameOfDirectory(new string('\0', 248));
    wrapper.WrapAppPath(new string('\0', 248));
    wrapper.WrapAppName("\0\0\0\0\0\0");
    wrapper.WrapMacAddress();
    wrapper.WrapReportType(0);
    this.WrapReportTypeTest(wrapper, 0);
    PexAssert.IsNotNull((object)wrapper);
}

[Test]
[PexGeneratedBy(typeof(WrapperTest))]
public void WrapReportTypeTest44701()
{
    Wrapper wrapper;
    wrapper = new Wrapper();
    wrapper.WrapComputerName();
    wrapper.WrapUserName();
    wrapper.WrapTimestamp(0);
    wrapper.WrapNameOfDirectory(
                               "\0\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\""
                               );
    wrapper.WrapAppPath(
                       "\0\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\""
                       );
    wrapper.WrapAppName("\0\"");
    wrapper.WrapMacAddress();
    wrapper.WrapReportType(0);
    this.WrapReportTypeTest(wrapper, 0);
    PexAssert.IsNotNull((object)wrapper);
}

[Test]
[PexGeneratedBy(typeof(WrapperTest))]
public void WrapReportTypeTest904()
{
    Wrapper wrapper;
    wrapper = new Wrapper();
    wrapper.WrapComputerName();
    wrapper.WrapUserName();
    wrapper.WrapTimestamp(0);
    wrapper.WrapNameOfDirectory(
                               "\0\0\0\0\0\0^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^"
                               );
    wrapper.WrapAppPath(
                       "\0\0\0\0\0\0^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^"
                       );
    wrapper.WrapAppName("\0\0\0\0\0\0");
    wrapper.WrapMacAddress();
    wrapper.WrapReportType(int.MinValue);
    this.WrapReportTypeTest(wrapper, int.MinValue);
    PexAssert.IsNotNull((object)wrapper);
}

[Test]
[PexGeneratedBy(typeof(WrapperTest))]
public void WrapReportTypeTest44702()
{
    Wrapper wrapper;
    wrapper = new Wrapper();
    wrapper.WrapComputerName();
    wrapper.WrapUserName();
    wrapper.WrapTimestamp(0);
    wrapper.WrapNameOfDirectory(
                               "\0~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
                               );
    wrapper.WrapAppPath("~");
    wrapper.WrapAppName("\0");
    wrapper.WrapMacAddress();
    wrapper.WrapReportType(0);
    this.WrapReportTypeTest(wrapper, 0);
    PexAssert.IsNotNull((object)wrapper);
}

[Test]
[PexGeneratedBy(typeof(WrapperTest))]
public void WrapReportTypeTest44703()
{
    Wrapper wrapper;
    wrapper = new Wrapper();
    wrapper.WrapComputerName();
    wrapper.WrapUserName();
    wrapper.WrapTimestamp(0);
    wrapper.WrapNameOfDirectory(
                               "&\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0"
                               );
    wrapper.WrapAppPath("~");
    wrapper.WrapAppName("&");
    wrapper.WrapMacAddress();
    wrapper.WrapReportType(0);
    this.WrapReportTypeTest(wrapper, 0);
    PexAssert.IsNotNull((object)wrapper);
}

[Test]
[PexGeneratedBy(typeof(WrapperTest))]
public void WrapReportTypeTest44704()
{
    Wrapper wrapper;
    wrapper = new Wrapper();
    wrapper.WrapComputerName();
    wrapper.WrapUserName();
    wrapper.WrapTimestamp(0);
    wrapper.WrapNameOfDirectory(
                               "                                                                                                                                                                                                                                                       "
                               );
    wrapper.WrapAppPath(" ");
    wrapper.WrapAppName(
                       "                                                                                                                                                                                                                                                                   "
                       );
    wrapper.WrapMacAddress();
    wrapper.WrapReportType(0);
    this.WrapReportTypeTest(wrapper, 0);
    PexAssert.IsNotNull((object)wrapper);
}

[Test]
[PexGeneratedBy(typeof(WrapperTest))]
public void WrapReportTypeTest44705()
{
    Wrapper wrapper;
    wrapper = new Wrapper();
    wrapper.WrapComputerName();
    wrapper.WrapUserName();
    wrapper.WrapTimestamp(946684800);
    wrapper.WrapNameOfDirectory("<");
    wrapper.WrapAppPath("");
    wrapper.WrapAppName("");
    wrapper.WrapMacAddress();
    wrapper.WrapReportType(0);
    this.WrapReportTypeTest(wrapper, 0);
    PexAssert.IsNotNull((object)wrapper);
}

[Test]
[PexGeneratedBy(typeof(WrapperTest))]
public void WrapReportTypeTest44706()
{
    Wrapper wrapper;
    wrapper = new Wrapper();
    wrapper.WrapComputerName();
    wrapper.WrapUserName();
    wrapper.WrapTimestamp(946684800);
    wrapper.WrapNameOfDirectory("<");
    wrapper.WrapAppPath("");
    wrapper.WrapAppName("<\0");
    wrapper.WrapMacAddress();
    wrapper.WrapReportType(0);
    this.WrapReportTypeTest(wrapper, 0);
    PexAssert.IsNotNull((object)wrapper);
}

[Test]
[PexGeneratedBy(typeof(WrapperTest))]
public void WrapReportTypeTest44707()
{
    Wrapper wrapper;
    wrapper = new Wrapper();
    wrapper.WrapComputerName();
    wrapper.WrapUserName();
    wrapper.WrapTimestamp(946684800);
    wrapper.WrapNameOfDirectory("<");
    wrapper.WrapAppPath("");
    wrapper.WrapAppName("<¿");
    wrapper.WrapMacAddress();
    wrapper.WrapReportType(0);
    this.WrapReportTypeTest(wrapper, 0);
    PexAssert.IsNotNull((object)wrapper);
}

[Test]
[PexGeneratedBy(typeof(WrapperTest))]
public void WrapReportTypeTest44708()
{
    Wrapper wrapper;
    wrapper = new Wrapper();
    wrapper.WrapComputerName();
    wrapper.WrapUserName();
    wrapper.WrapTimestamp(0);
    wrapper.WrapNameOfDirectory(
                               "�\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0"
                               );
    wrapper.WrapAppPath("\\ ");
    wrapper.WrapAppName("�");
    wrapper.WrapMacAddress();
    wrapper.WrapReportType(0);
    this.WrapReportTypeTest(wrapper, 0);
    PexAssert.IsNotNull((object)wrapper);
}

[Test]
[PexGeneratedBy(typeof(WrapperTest))]
public void WrapReportTypeTest44709()
{
    Wrapper wrapper;
    wrapper = new Wrapper();
    wrapper.WrapComputerName();
    wrapper.WrapUserName();
    wrapper.WrapTimestamp(0);
    wrapper.WrapNameOfDirectory(
                               "�\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0"
                               );
    wrapper.WrapAppPath("\\ ");
    wrapper.WrapAppName("�\0");
    wrapper.WrapMacAddress();
    wrapper.WrapReportType(0);
    this.WrapReportTypeTest(wrapper, 0);
    PexAssert.IsNotNull((object)wrapper);
}

[Test]
[PexGeneratedBy(typeof(WrapperTest))]
public void WrapReportTypeTest44720()
{
    Wrapper wrapper;
    wrapper = new Wrapper();
    wrapper.WrapComputerName();
    wrapper.WrapUserName();
    wrapper.WrapTimestamp(0);
    wrapper.WrapNameOfDirectory(
                               "00<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<"
                               );
    wrapper.WrapAppPath("00");
    wrapper.WrapAppName("<\0\0\0<<");
    wrapper.WrapMacAddress();
    wrapper.WrapReportType(0);
    this.WrapReportTypeTest(wrapper, 0);
    PexAssert.IsNotNull((object)wrapper);
}

[Test]
[PexGeneratedBy(typeof(WrapperTest))]
[PexRaisedException(typeof(TermDestructionException))]
public void WrapReportTypeTestThrowsTermDestructionException840()
{
    Wrapper wrapper;
    wrapper = new Wrapper();
    wrapper.WrapComputerName();
    wrapper.WrapUserName();
    wrapper.WrapTimestamp(0);
    wrapper.WrapNameOfDirectory(
                               "<\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0"
                               );
    wrapper.WrapAppPath(".");
    wrapper.WrapAppName("<");
    wrapper.WrapMacAddress();
    wrapper.WrapReportType(0);
    this.WrapReportTypeTest(wrapper, 0);
}

[Test]
[PexGeneratedBy(typeof(WrapperTest))]
[Ignore("the test state was: path bounds exceeded")]
public void WrapReportTypeTest01()
{
    Wrapper wrapper;
    wrapper = new Wrapper();
    wrapper.WrapComputerName();
    wrapper.WrapUserName();
    wrapper.WrapTimestamp(0);
    wrapper.WrapNameOfDirectory(
                               "\f .Ā ࠀ䀀 䀀ࠀȀĀ   Ѐ က䀀ȀĀࠀࠀ耀Ȁ@ကȀ耀Ѐက@䀀 ࠀĀ䀀ࠀȀȀȀȀ耀Ѐ\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0"
                               );
    wrapper.WrapAppPath("\f .Ā ࠀ䀀 䀀ࠀȀĀ   Ѐ က䀀ȀĀࠀࠀ耀Ȁ@ကȀ耀Ѐက@䀀 ࠀĀ䀀ࠀȀȀȀȀ耀Ѐ");
    wrapper.WrapAppName(
                       "\f .Ā ࠀ䀀 䀀ࠀȀĀ   Ѐ က䀀ȀĀࠀࠀ耀Ȁ@ကȀ耀Ѐက@䀀 ࠀĀ䀀ࠀȀȀȀȀ耀Ѐ\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0"
                       );
    wrapper.WrapMacAddress();
    wrapper.WrapReportType(0);
    this.WrapReportTypeTest(wrapper, 0);
}
    }
}
