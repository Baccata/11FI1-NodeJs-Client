// <copyright file="LogTest.cs" company="NodeJsTeam">Copyright © NodeJsTeam 2015</copyright>

using System;
using Microsoft.Pex.Framework;
using Microsoft.Pex.Framework.Validation;
using NUnit.Framework;
using NodeJsClientService;

namespace NodeJsClientService.Tests
{
    /// <summary>This class contains parameterized unit tests for Log</summary>
    [TestFixture]
    [PexClass(typeof(Log))]
    [PexAllowedExceptionFromTypeUnderTest(typeof(ArgumentException), AcceptExceptionSubtypes = true)]
    [PexAllowedExceptionFromTypeUnderTest(typeof(InvalidOperationException))]
    public partial class LogTest
    {

        /// <summary>Test stub for DoLogging(String)</summary>
        [PexMethod]
        public void DoLoggingTest(string message)
        {
            Log.DoLogging(message);
            // TODO: add assertions to method LogTest.DoLoggingTest(String)
        }
    }
}
