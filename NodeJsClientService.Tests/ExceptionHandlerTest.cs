using System.Reflection;
// <copyright file="ExceptionHandlerTest.cs" company="NodeJsTeam">Copyright © NodeJsTeam 2015</copyright>

using System;
using Microsoft.Pex.Framework;
using Microsoft.Pex.Framework.Validation;
using NUnit.Framework;
using NodeJsClientService;

namespace NodeJsClientService.Tests
{
    /// <summary>This class contains parameterized unit tests for ExceptionHandler</summary>
    [TestFixture]
    [PexClass(typeof(ExceptionHandler))]
    [PexAllowedExceptionFromTypeUnderTest(typeof(ArgumentException), AcceptExceptionSubtypes = true)]
    [PexAllowedExceptionFromTypeUnderTest(typeof(InvalidOperationException))]
    public partial class ExceptionHandlerTest
    {

        /// <summary>Test stub for HandleException(Exception, String, Boolean)</summary>
        [PexMethod]
        public void HandleExceptionTest(
            Exception ex,
            string errorMessage,
            bool increaseCounter
        )
        {
            ExceptionHandler.HandleException(ex, errorMessage, increaseCounter);
            // TODO: add assertions to method ExceptionHandlerTest.HandleExceptionTest(Exception, String, Boolean)
        }

        [PexMethod]
        [PexMethodUnderTest("LogErrorMessage(Exception, String)")]
        [PexAllowedException(typeof(TargetInvocationException))]
        [PexAllowedException(typeof(TargetInvocationException))]
        internal void LogErrorMessage(Exception ex, string errorMessage)
        {
            object[] args = new object[2];
            args[0] = (object)ex;
            args[1] = (object)errorMessage;
            Type[] parameterTypes = new Type[2];
            parameterTypes[0] = typeof(Exception);
            parameterTypes[1] = typeof(string);
            object result = ((MethodBase)(typeof(ExceptionHandler).GetMethod("LogErrorMessage",
                                                                             BindingFlags.DeclaredOnly | BindingFlags.Static | BindingFlags.NonPublic, (Binder)null,
                                                                             CallingConventions.Standard, parameterTypes, (ParameterModifier[])null)))
                                .Invoke((object)null, args);
            // TODO: add assertions to method ExceptionHandlerTest.LogErrorMessage(Exception, String)
        }

        [PexMethod]
        [PexMethodUnderTest("IncreaseCounter(Boolean)")]
        internal void IncreaseCounter(bool increaseCounter)
        {
            object[] args = new object[1];
            args[0] = (object)increaseCounter;
            Type[] parameterTypes = new Type[1];
            parameterTypes[0] = typeof(bool);
            object result = ((MethodBase)(typeof(ExceptionHandler).GetMethod("IncreaseCounter",
                                                                             BindingFlags.DeclaredOnly | BindingFlags.Static | BindingFlags.NonPublic, (Binder)null,
                                                                             CallingConventions.Standard, parameterTypes, (ParameterModifier[])null)))
                                .Invoke((object)null, args);
            // TODO: add assertions to method ExceptionHandlerTest.IncreaseCounter(Boolean)
        }

        [PexMethod]
        [PexMethodUnderTest("AreArgumentsValide(Exception, String)")]
        internal bool AreArgumentsValide(Exception ex, string errorMessage)
        {
            object[] args = new object[2];
            args[0] = (object)ex;
            args[1] = (object)errorMessage;
            Type[] parameterTypes = new Type[2];
            parameterTypes[0] = typeof(Exception);
            parameterTypes[1] = typeof(string);
            bool result0 = (bool)(((MethodBase)(typeof(ExceptionHandler).GetMethod("AreArgumentsValide",
                                                                                   BindingFlags.DeclaredOnly | BindingFlags.Static | BindingFlags.NonPublic, (Binder)null,
                                                                                   CallingConventions.Standard, parameterTypes, (ParameterModifier[])null)))
                                      .Invoke((object)null, args));
            bool result = result0;
            return result;
            // TODO: add assertions to method ExceptionHandlerTest.AreArgumentsValide(Exception, String)
        }
    }
}
