// <copyright file="ProgramPathsTest.cs" company="NodeJsTeam">Copyright © NodeJsTeam 2015</copyright>

using System;
using Microsoft.Pex.Framework;
using Microsoft.Pex.Framework.Validation;
using NUnit.Framework;
using NodeJsClientService;

namespace NodeJsClientService.Tests
{
    /// <summary>This class contains parameterized unit tests for ProgramPaths</summary>
    [TestFixture]
    [PexClass(typeof(ProgramUtils))]
    [PexAllowedExceptionFromTypeUnderTest(typeof(ArgumentException), AcceptExceptionSubtypes = true)]
    [PexAllowedExceptionFromTypeUnderTest(typeof(InvalidOperationException))]
    public partial class ProgramUtilsTest
    {

        /// <summary>Test stub for get_CurrentMacAddress()</summary>
        [PexMethod]
        internal string CurrentMacAddressGetTest()
        {
            string result = ProgramUtils.CurrentMacAddress;
            return result;
            // TODO: add assertions to method ProgramPathsTest.CurrentMacAddressGetTest()
        }

        /// <summary>Test stub for get_CurrentUser()</summary>
        [PexMethod]
        internal string CurrentUserGetTest()
        {
            string result = ProgramUtils.CurrentUserName;
            return result;
            // TODO: add assertions to method ProgramPathsTest.CurrentUserGetTest()
        }
    }
}
