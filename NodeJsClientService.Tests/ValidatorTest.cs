using System.Reflection;
// <copyright file="ValidatorTest.cs" company="NodeJsTeam">Copyright © NodeJsTeam 2015</copyright>

using System;
using Microsoft.Pex.Framework;
using Microsoft.Pex.Framework.Validation;
using NUnit.Framework;
using NodeJsClientService;

namespace NodeJsClientService.Tests
{
    /// <summary>This class contains parameterized unit tests for Validator</summary>
    [TestFixture]
    [PexClass(typeof(Validator))]
    [PexAllowedExceptionFromTypeUnderTest(typeof(ArgumentException), AcceptExceptionSubtypes = true)]
    [PexAllowedExceptionFromTypeUnderTest(typeof(InvalidOperationException))]
    public partial class ValidatorTest
    {

        /// <summary>Test stub for ValidateAppName(String)</summary>
        [PexMethod]
        internal bool ValidateAppNameTest([PexAssumeUnderTest]Validator target, string appName)
        {
            bool result = target.ValidateAppName(appName);
            return result;
            // TODO: add assertions to method ValidatorTest.ValidateAppNameTest(Validator, String)
        }

        /// <summary>Test stub for ValidateAppPath(String)</summary>
        [PexMethod]
        internal bool ValidateAppPathTest([PexAssumeUnderTest]Validator target, string appPath)
        {
            bool result = target.ValidateAppPath(appPath);
            return result;
            // TODO: add assertions to method ValidatorTest.ValidateAppPathTest(Validator, String)
        }

        /// <summary>Test stub for ValidateComputerName(String)</summary>
        [PexMethod]
        internal bool ValidateComputerNameTest([PexAssumeUnderTest]Validator target, string computerName)
        {
            bool result = target.ValidateComputerName(computerName);
            return result;
            // TODO: add assertions to method ValidatorTest.ValidateComputerNameTest(Validator, String)
        }

        /// <summary>Test stub for ValidateMacAddress(String)</summary>
        [PexMethod]
        internal bool ValidateMacAddressTest([PexAssumeUnderTest]Validator target, string macAddress)
        {
            bool result = target.ValidateMacAddress(macAddress);
            return result;
            // TODO: add assertions to method ValidatorTest.ValidateMacAddressTest(Validator, String)
        }

        /// <summary>Test stub for ValidateNameOfDirectory(String)</summary>
        [PexMethod]
        internal bool ValidateNameOfDirectoryTest([PexAssumeUnderTest]Validator target, string nameOfDirectory)
        {
            bool result = target.ValidateNameOfDirectory(nameOfDirectory);
            return result;
            // TODO: add assertions to method ValidatorTest.ValidateNameOfDirectoryTest(Validator, String)
        }

        /// <summary>Test stub for ValidateReportType(Int32)</summary>
        [PexMethod]
        internal bool ValidateReportTypeTest([PexAssumeUnderTest]Validator target, int reportType)
        {
            bool result = target.ValidateReportType(reportType);
            return result;
            // TODO: add assertions to method ValidatorTest.ValidateReportTypeTest(Validator, Int32)
        }

        /// <summary>Test stub for ValidateTimestamp(Int32)</summary>
        [PexMethod]
        internal bool ValidateTimestampTest([PexAssumeUnderTest]Validator target, int timestamp)
        {
            bool result = target.ValidateTimestamp(timestamp);
            return result;
            // TODO: add assertions to method ValidatorTest.ValidateTimestampTest(Validator, Int32)
        }

        /// <summary>Test stub for ValidateUserName(String)</summary>
        [PexMethod]
        internal bool ValidateUserNameTest([PexAssumeUnderTest]Validator target, string userName)
        {
            bool result = target.ValidateUserName(userName);
            return result;
            // TODO: add assertions to method ValidatorTest.ValidateUserNameTest(Validator, String)
        }

        [PexMethod(MaxRunsWithoutNewTests = 200)]
        [PexMethodUnderTest("CheckPathWrapper(String)")]
        internal bool CheckPathWrapper([PexAssumeUnderTest]Validator target, string appPath)
        {
            object[] args = new object[1];
            args[0] = (object)appPath;
            Type[] parameterTypes = new Type[1];
            parameterTypes[0] = typeof(string);
            bool result0 = (bool)(((MethodBase)(typeof(Validator).GetMethod("CheckPathWrapper",
                                                                            BindingFlags.DeclaredOnly | BindingFlags.Instance | BindingFlags.NonPublic,
                                                                            (Binder)null, CallingConventions.HasThis, parameterTypes, (ParameterModifier[])null)))
                                      .Invoke((object)target, args));
            bool result = result0;
            return result;
            // TODO: add assertions to method ValidatorTest.CheckPathWrapper(Validator, String)
        }
    }
}
