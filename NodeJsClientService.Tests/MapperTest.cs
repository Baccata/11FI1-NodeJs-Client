// <copyright file="MapperTest.cs" company="NodeJsTeam">Copyright © NodeJsTeam 2015</copyright>

using System;
using Microsoft.Pex.Framework;
using Microsoft.Pex.Framework.Validation;
using NUnit.Framework;
using NodeJsClientService;

namespace NodeJsClientService.Tests
{
    /// <summary>This class contains parameterized unit tests for Mapper</summary>
    [TestFixture]
    [PexClass(typeof(Mapper))]
    [PexAllowedExceptionFromTypeUnderTest(typeof(ArgumentException), AcceptExceptionSubtypes = true)]
    [PexAllowedExceptionFromTypeUnderTest(typeof(InvalidOperationException))]
    public partial class MapperTest
    {

        /// <summary>Test stub for TriggerMapping(String, String)</summary>
        [PexMethod]
        internal string TriggerMappingTest(
            [PexAssumeUnderTest]Mapper target,
            string nameOfDirectory,
            string fileName
        )
        {
            string result = target.TriggerMapping(nameOfDirectory, fileName);
            return result;
            // TODO: add assertions to method MapperTest.TriggerMappingTest(Mapper, String, String)
        }
    }
}
