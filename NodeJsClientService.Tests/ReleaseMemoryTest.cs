// <copyright file="ReleaseMemoryTest.cs" company="NodeJsTeam">Copyright © NodeJsTeam 2015</copyright>

using System;
using Microsoft.Pex.Framework;
using Microsoft.Pex.Framework.Validation;
using NUnit.Framework;
using NodeJsClientService;

namespace NodeJsClientService.Tests
{
    /// <summary>This class contains parameterized unit tests for ReleaseMemory</summary>
    [TestFixture]
    [PexClass(typeof(ReleaseMemory))]
    [PexAllowedExceptionFromTypeUnderTest(typeof(ArgumentException), AcceptExceptionSubtypes = true)]
    [PexAllowedExceptionFromTypeUnderTest(typeof(InvalidOperationException))]
    public partial class ReleaseMemoryTest
    {

        /// <summary>Test stub for FreeRam()</summary>
        [PexMethod]
        internal void FreeRamTest()
        {
            ReleaseMemory.FreeRam();
            // TODO: add assertions to method ReleaseMemoryTest.FreeRamTest()
        }
    }
}
