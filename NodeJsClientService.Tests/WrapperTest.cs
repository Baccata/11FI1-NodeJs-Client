// <copyright file="WrapperTest.cs" company="NodeJsTeam">Copyright © NodeJsTeam 2015</copyright>

using System;
using Microsoft.Pex.Framework;
using Microsoft.Pex.Framework.Validation;
using NUnit.Framework;
using NodeJsClientService;

namespace NodeJsClientService.Tests
{
    /// <summary>This class contains parameterized unit tests for Wrapper</summary>
    [TestFixture]
    [PexClass(typeof(Wrapper))]
    [PexAllowedExceptionFromTypeUnderTest(typeof(ArgumentException), AcceptExceptionSubtypes = true)]
    [PexAllowedExceptionFromTypeUnderTest(typeof(InvalidOperationException))]
    public partial class WrapperTest
    {

        /// <summary>Test stub for .ctor()</summary>
        [PexMethod]
        internal Wrapper ConstructorTest()
        {
            Wrapper target = new Wrapper();
            return target;
            // TODO: add assertions to method WrapperTest.ConstructorTest()
        }

        /// <summary>Test stub for GetValidJsonObject()</summary>
        [PexMethod(MaxRunsWithoutNewTests = 20000)]
        internal JsonProperties GetValidJsonObjectTest([PexAssumeUnderTest]Wrapper target)
        {
            JsonProperties result = target.GetValidJsonObject();
            return result;
            // TODO: add assertions to method WrapperTest.GetValidJsonObjectTest(Wrapper)
        }

        /// <summary>Test stub for WrapAppName(String)</summary>
        [PexMethod]
        internal void WrapAppNameTest([PexAssumeUnderTest]Wrapper target, string appName)
        {
            target.WrapAppName(appName);
            // TODO: add assertions to method WrapperTest.WrapAppNameTest(Wrapper, String)
        }

        /// <summary>Test stub for WrapAppPath(String)</summary>
        [PexMethod]
        internal void WrapAppPathTest([PexAssumeUnderTest]Wrapper target, string appPath)
        {
            target.WrapAppPath(appPath);
            // TODO: add assertions to method WrapperTest.WrapAppPathTest(Wrapper, String)
        }

        /// <summary>Test stub for WrapComputerName()</summary>
        [PexMethod]
        internal void WrapComputerNameTest([PexAssumeUnderTest]Wrapper target)
        {
            target.WrapComputerName();
            // TODO: add assertions to method WrapperTest.WrapComputerNameTest(Wrapper)
        }

        /// <summary>Test stub for WrapMacAddress()</summary>
        [PexMethod]
        internal void WrapMacAddressTest([PexAssumeUnderTest]Wrapper target)
        {
            target.WrapMacAddress();
            // TODO: add assertions to method WrapperTest.WrapMacAddressTest(Wrapper)
        }

        /// <summary>Test stub for WrapNameOfDirectory(String)</summary>
        [PexMethod]
        internal void WrapNameOfDirectoryTest([PexAssumeUnderTest]Wrapper target, string nameOfDirectory)
        {
            target.WrapNameOfDirectory(nameOfDirectory);
            // TODO: add assertions to method WrapperTest.WrapNameOfDirectoryTest(Wrapper, String)
        }

        /// <summary>Test stub for WrapReportType(Int32)</summary>
        [PexMethod(MaxRunsWithoutNewTests = 2000)]
        internal void WrapReportTypeTest([PexAssumeUnderTest]Wrapper target, int reportType)
        {
            target.WrapReportType(reportType);
            // TODO: add assertions to method WrapperTest.WrapReportTypeTest(Wrapper, Int32)
        }

        /// <summary>Test stub for WrapTimestamp(Int32)</summary>
        [PexMethod]
        internal void WrapTimestampTest([PexAssumeUnderTest]Wrapper target, int creationTimestamp)
        {
            target.WrapTimestamp(creationTimestamp);
            // TODO: add assertions to method WrapperTest.WrapTimestampTest(Wrapper, Int32)
        }

        /// <summary>Test stub for WrapUserName()</summary>
        [PexMethod]
        internal void WrapUserNameTest([PexAssumeUnderTest]Wrapper target)
        {
            target.WrapUserName();
            // TODO: add assertions to method WrapperTest.WrapUserNameTest(Wrapper)
        }
    }
}
