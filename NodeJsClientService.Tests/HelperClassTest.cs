// <copyright file="HelperClassTest.cs" company="NodeJsTeam">Copyright © NodeJsTeam 2015</copyright>

using System;
using Microsoft.Pex.Framework;
using Microsoft.Pex.Framework.Validation;
using NUnit.Framework;
using NodeJsClientService;

namespace NodeJsClientService.Tests
{
    /// <summary>This class contains parameterized unit tests for HelperClass</summary>
    [TestFixture]
    [PexClass(typeof(HelperClass))]
    [PexAllowedExceptionFromTypeUnderTest(typeof(ArgumentException), AcceptExceptionSubtypes = true)]
    [PexAllowedExceptionFromTypeUnderTest(typeof(InvalidOperationException))]
    public partial class HelperClassTest
    {

        /// <summary>Test stub for CheckDirectory(String)</summary>
        [PexMethod]
        internal bool CheckDirectoryTest(string path)
        {
            bool result = HelperClass.CheckDirectory(path);
            return result;
            // TODO: add assertions to method HelperClassTest.CheckDirectoryTest(String)
        }

        /// <summary>Test stub for CheckIfFileExists(String)</summary>
        [PexMethod]
        internal bool CheckIfFileExistsTest(string pathToFile)
        {
            bool result = HelperClass.CheckIfFileExists(pathToFile);
            return result;
            // TODO: add assertions to method HelperClassTest.CheckIfFileExistsTest(String)
        }
    }
}
