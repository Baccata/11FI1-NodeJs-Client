﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NodeJsClientService
{
    //handle and process exeptions
    internal static class ExceptionHandler
    {
        internal static int errorCounter = 0;

        //get exception (ex) and error message (string)
        internal static void HandleException(Exception ex, string errorMessage, bool increaseCounter = true)
        {
            IncreaseCounter(increaseCounter);

            LogErrorMessage(ex, errorMessage);
#if DEBUG
            WriteErrorMessageToConsole(ex, errorMessage);
#endif
        }

        //increase errorCounter if true
        private static void IncreaseCounter(bool increaseCounter)
        {
            if (increaseCounter)
            {
                errorCounter++;
            }
        }

        //write error message to log file
        private static void LogErrorMessage(Exception ex, string errorMessage)
        {
            if (ex.InnerException != null)
            {
                Log.DoLogging("Error: " + errorMessage + ": " + ex.ToString() + " ||| InnerException: " + ex.InnerException.ToString());
            }
            else
            {
                Log.DoLogging("Error: " + errorMessage + ": " + ex.ToString());
            }
        }

        //write error message to console
        private static void WriteErrorMessageToConsole(Exception ex, string errorMessage)
        {
            if (ex.InnerException != null)
            {
                Console.WriteLine("Error: {0}: {1} - InnerException: {2}", errorMessage, ex.ToString(), ex.InnerException.ToString());
            }
            else
            {
                Console.WriteLine("Error: {0}: {1}", errorMessage, ex.ToString());
            }
        }
    }
}