﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using NodeJsClientService;

namespace WatchDog
{
    //up we go
    public static class Program
    {
        private static Mutex _mutex = new Mutex(true, "{D18C6517-DF21-4C7D-9ECA-B1EC38F0C2D6}");
        private static Controller _controller = new Controller();
        private static Process _current = Process.GetCurrentProcess();
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        public static void Main()
        {
            try
            {
                CheckIfProgramIsAlreadyRunning();
            }
            catch (Exception ex)
            {
                ExceptionHandler.HandleException(ex, "unable to launch watchdog");
            }
        }

        //check if program is already running -> if not run program
        private static void CheckIfProgramIsAlreadyRunning()
        {
            if (_mutex.WaitOne(TimeSpan.Zero, true))
            {
                StartWatchDog();
                _mutex.ReleaseMutex();
            }
            else
            {
                // OpenRunningInstance();
                // RestartWatchDog();
            }
        }

        //bring already running instance of program to foreground
        private static void OpenRunningInstance()
        {
            foreach (Process process in Process.GetProcessesByName(_current.ProcessName))
            {
                ShowWindow(process);
                break;
            }
        }

        //set window of alredy running instance to foreground
        private static void ShowWindow(Process process)
        {
            if (process.Id != _current.Id)
            {
                //change minimized window to normal
                NativeMethods.ShowWindow(process.MainWindowHandle, 1);  //1 == SW_SHOWNORMAL

                //set window to foreground
                NativeMethods.SetForegroundWindow(process.MainWindowHandle);
            }
        }

        private static void RestartWatchDog()
        {
            Environment.Exit(1);
            StartWatchDog();
        }

        private static void StartWatchDog()
        {
            _controller.Start();
        }
    }
}