﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NodeJsClientService;

namespace WatchDog
{
    internal static class Main
    {
        internal static string storageDirectory = Environment.GetEnvironmentVariable("LocalAppData") + @"\WerWatchDog";

        private static string localAppData = Environment.GetEnvironmentVariable("LocalAppData");
        internal static string watchDogDirectory = localAppData + @"\Microsoft\Windows\WER\ReportArchive\";
    }
}