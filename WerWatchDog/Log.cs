﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using NodeJsClientService;
using WatchDog;

namespace NodeJsClientService
{
    //log events in txt file
    public static class Log
    {
        private static string _logPath = Main.storageDirectory + @"\log\";
        private static string _logFile = logPath + "log.txt";
        private static string logPath
        {
            get { return _logPath; }
        }
        internal static string logFile
        {
            get { return _logFile; }
        }

        //process input message
        public static void DoLogging(string message)
        {
            try
            {
                CheckIfDirectoryExists();
                CheckIfLogFileExists();

                WriteMessageInLogFile(message);
            }
#if DEBUG
            catch (Exception ex)
            {
                Console.WriteLine("unable to create log. Cause: " + ex.ToString());
            }
#else
            catch { }
#endif
        }

        //check and create directory if it not exists
        private static void CheckIfDirectoryExists()
        {
            HelperClass.CheckDirectory(logPath);
        }

        //check and create log file if it not exists
        private static void CheckIfLogFileExists()
        {
            if (!HelperClass.CheckIfFileExists(logFile))
            {
                WriteHeaderInLogFile();
            }
        }

        //write header in log file
        private static void WriteHeaderInLogFile()
        {
            using (StreamWriter writer = File.AppendText(logFile))
            {
                InitializeLogFile(writer);
            }
        }

        //write message in log file
        private static void WriteMessageInLogFile(string message)
        {
            using (StreamWriter writer = File.AppendText(logFile))
            {
                LoggingMessage(writer, message);
            }
        }

        //write message with timestamp in log file
        private static void LoggingMessage(TextWriter txtWriter, string logMessage)
        {
            try
            {
                CheckForLastWriteTime(txtWriter);

                //write message in log file
                txtWriter.WriteLine("{0} - {1} | {2}", DateTime.Now.ToShortDateString(), DateTime.Now.ToLongTimeString(), logMessage);
            }
#if DEBUG
            catch (Exception ex)
            {
                Console.WriteLine("unable to write message into log. Cause: {0}", ex.ToString());
            }
#else
            catch { }
#endif
        }

        //if log file has not been written within 3 hours -> add horizontal ruler
        private static void CheckForLastWriteTime(TextWriter txtWriter)
        {
            //get last write time of log file
            DateTime logFileLastWriteTimeUtc = File.GetLastWriteTimeUtc(logPath);

            if (logFileLastWriteTimeUtc < DateTime.UtcNow.AddHours(-3))
            {
                AddHorizontalRuler(txtWriter);
            }
        }

        //write header in log file
        private static void InitializeLogFile(TextWriter txtWriter)
        {
            try
            {
                txtWriter.WriteLine("Node.js Client WatchDog Log:");
                AddHorizontalRuler(txtWriter);
                txtWriter.WriteLine("DD.MM.YYYY - HH:MM:SS | Message");
                AddHorizontalRuler(txtWriter);
            }
#if DEBUG
            catch (Exception ex)
            {
                Console.WriteLine("unable to initialize log. Cause: {0}", ex.ToString());
            }
#else
            catch { }
#endif
        }

        //write horizontal ruler in log file
        private static void AddHorizontalRuler(TextWriter txtWriter)
        {
            try
            {
                txtWriter.WriteLine("-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-");
            }
#if DEBUG
            catch (Exception ex)
            {
                Console.WriteLine("unable to write horizontal ruler. Cause: {0}", ex.ToString());
            }
#else
            catch { }
#endif
        }
    }
}