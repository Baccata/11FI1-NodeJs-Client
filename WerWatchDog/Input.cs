﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Diagnostics;
using System.Threading;
using NodeJsClientService;

namespace WatchDog
{
    //watches selected directory for changes
    internal class Input
    {
        #region variables
        private SendInfoToServer sendInfo = new SendInfoToServer();
        private Thread _mainThread = null;
        private Thread _machineOffFiles = null;
        private Thread _keepAnEyeAtRamUsage = null;
        private Dictionary<string, string> _dictWithFolders = new Dictionary<string, string>();
        private int _counter = 0;
        #endregion

        //point of entry
        internal void Start()
        {
            try
            {
                Log.DoLogging("program started");

                SetThreads();

                InitializeProgram();
            }
            catch (Exception ex)
            {
                ExceptionHandler.HandleException(ex, ex.Message);
            }
        }

        #region Threads
        //set all threads
        private void SetThreads()
        {
            SetMainThread();
            SetRamUsageThread();
            SetTrigger();
        }

        //set mainThread
        private void SetMainThread()
        {
            _mainThread = Thread.CurrentThread;
        }

        //watch out for high memory usage
        private void SetRamUsageThread()
        {
            _keepAnEyeAtRamUsage = new Thread(new ThreadStart(this.WatchRamUsage));
            _keepAnEyeAtRamUsage.Priority = ThreadPriority.BelowNormal;
            _keepAnEyeAtRamUsage.IsBackground = true;
            _keepAnEyeAtRamUsage.Start();
        }

        //triggers file transfer if file has been spotted
        private void SetTrigger()
        {
            _machineOffFiles = new Thread(new ThreadStart(this.IsThereAnyWorkToDo));
            _machineOffFiles.Priority = ThreadPriority.BelowNormal;
            _machineOffFiles.IsBackground = true;
            _machineOffFiles.Start();
        }
        #endregion

        //startup
        private void InitializeProgram()
        {
            try
            {
                ActivateWatcher();
            }
            catch (Exception ex)
            {
                ExceptionHandler.HandleException(ex, ex.Message);
            }

            //don't close watchdog
            while (true)
            {
                Thread.Sleep(1);
            }
        }

        //set & activate watcher
        private void ActivateWatcher()
        {
            //set watcher
            FileSystemWatcher watcher = new FileSystemWatcher();

            //set path
            watcher.Path = Paths.watchDogDirectory;

            //add delgate
            watcher.Created += new FileSystemEventHandler(OnChanged);

            //run watcher
            watcher.EnableRaisingEvents = true;
        }

        //triggers if a new file has been added
        private void OnChanged(object source, FileSystemEventArgs e)
        {
            try
            {
                CheckFlagOfFile(e);
            }
            catch (Exception ex)
            {
                ExceptionHandler.HandleException(ex, ex.Message);
            }
        }

        //read flag info of changed item (file/folder
        private void CheckFlagOfFile(FileSystemEventArgs e)
        {
            //read file attributes
            FileAttributes attr = File.GetAttributes(e.FullPath);

            //check if new item is folder and compressed (blue written in explorer)
            if (attr.HasFlag(FileAttributes.Directory) && attr.HasFlag(FileAttributes.Compressed))
            {
                AddItemsToDict(e);
            }
        }

        //add item (key = folder name string | value = filename path+name string) to dictionary
        private void AddItemsToDict(FileSystemEventArgs e)
        {
            string pathOfDirectory = Path.GetFileName(e.FullPath);
            string fileName = e.FullPath + @"\Report.wer";

            this._dictWithFolders.Add(pathOfDirectory, fileName);
        }

        //check if there are files to transfer
        private void IsThereAnyWorkToDo()
        {
            while (this._mainThread.IsAlive)
            {
                if (this._dictWithFolders.Count > 0)
                {
                    HandleWork();
                }
                Thread.Sleep(1);
            }
        }

        //process dictionary entries
        private void HandleWork()
        {
            try
            {
                foreach (KeyValuePair<string, string> entry in this._dictWithFolders)
                {
                    //handle files one by one
                    ProcessFile(entry.Key, entry.Value);
                }
                ResetDictAndCounter();
            }
            catch (Exception ex)
            {
                HandleErrorsWhileWorking(ex);
            }
        }
            
        //on error increase counter and restart method after 1 second
        private void HandleErrorsWhileWorking(Exception ex)
        {
            this._counter++;
            if (this._counter <= 4)
            {
                Thread.Sleep(1000);
                HandleWork();
            }
            else
            {
                ExceptionHandler.HandleException(ex, ex.Message);
            }
        }

        //Cleanup after work finished
        private void ResetDictAndCounter()
        {
            this._dictWithFolders.Clear();
            this._counter = 0;
        }

        //do something with the file
        private bool ProcessFile(string pathOfDirectory, string fileName)
        {
            //trigger transport to server
            bool success = CheckIfFileExists(pathOfDirectory, fileName);
            
            //delete directory after transfer to server
            bool deletePossible = DeleteDirectory(fileName);

            if (success && deletePossible)
            {
                return true;
            }
            return false;
        }

        //check if file to transfer exists
        private bool CheckIfFileExists(string pathOfDirectory, string fileName)
        {
            if (File.Exists(fileName))
            {
                return TriggerTransferToServer(pathOfDirectory, fileName);
            }
            return false;
        }

        //triggers transfer to server
        private bool TriggerTransferToServer(string pathOfDirectory, string fileName)
        {
            FileInfo info = new FileInfo(fileName);

            //wait until file is completely created
            while (IsFileLocked(info) == false)
            {
                //trigger transport to server
                return this.sendInfo.Send(pathOfDirectory, fileName);
            }
            return false;
        }

        //delete file - returns true if delete was possible
        private bool DeleteDirectory(string fileName)
        {
            try
            {
                string pathOfDirectory = Path.GetDirectoryName(fileName);

                //delete directory with all subfolders and files in it
                Directory.Delete(pathOfDirectory, true);
                return true;
            }
            catch
            {
                return false;
            }
        }

        //check if file is locked (true) or available (true)
        private bool IsFileLocked(FileInfo file)
        {
            try
            {
                using (FileStream stream = file.Open(FileMode.Open, FileAccess.ReadWrite, FileShare.None))
                {
                    stream.Close();
                }
            }
            catch (IOException)
            {
                //file is unavailable
                Thread.Sleep(1000);
                return true;
            }
            catch (Exception)
            {
                return true;
            }
            //file is not locked - hurray
            return false;
        }

        //release unused RAM every 15 seconds
        private void WatchRamUsage()
        {
            while (this._mainThread.IsAlive)
            {
                //wait 1 Minute
                Thread.Sleep(60000);

                //release memory useage
                ReleaseMemory.FreeRam();
            }
        }
    }
}