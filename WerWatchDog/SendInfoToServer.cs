﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using WatchDog;

namespace NodeJsClientService
{
    internal class SendInfoToServer
    {
        #region variables
        private HttpWebRequest _request = null;
        private readonly byte[] _newLine = Encoding.UTF8.GetBytes(Environment.NewLine);
        private byte[] _fileName = null;
        private byte[] _data = null;

        //url of server to send file
        private const string _url = @"https://push.tweakl.com:443/";
#if DEBUG
        private readonly string _tempFile = Main.storageDirectory + @"\temp.txt";
#endif
        //get name of client pc
        private readonly string _computerName = Environment.MachineName;
        //get name of client user
        private readonly string _userName = Environment.UserName;

        private readonly Encoding _enc = Encoding.GetEncoding("iso-8859-1");
        #endregion

        //start sending data to server
        internal bool Send(string nameOfDirectory, string file)
        {
            try
            {
                //gathering info
                InitializeTransfer(nameOfDirectory, file);

                //transfer data to server
                SendToServer();
#if DEBUG
                //delete temp file
                if (File.Exists(this._tempFile))
                {
                    File.Delete(this._tempFile);
                }
#endif
                //get server response
                return GetServerResponse(this._request);
            }
            catch (Exception ex)
            {
                ExceptionHandler.HandleException(ex, "unable to send data to server");
                return false;
            }
        }

        #region Initialize connection and data to transfer
        private void InitializeTransfer(string nameOfDirectory, string file)
        {
            //gather information to send
            SetDirectoryName(nameOfDirectory);
            SetReportContentData(file);

            //set transfer details
            CreateHttpWebRequest();
        }

        //create and set directory name
        private void SetDirectoryName(string nameOfDirectory)
        {
            //set name of folder
            string data = "Filename=" + nameOfDirectory;
            this._fileName = Encoding.UTF8.GetBytes(data);
#if DEBUG            
            Console.WriteLine(data);
#endif
        }

        //create and set file content data with UTF8 file content
        private void SetReportContentData(string file)
        {
            //read content of file
            string stringWithInputEncoding = this._enc.GetString(File.ReadAllBytes(file));

            //get rid of spaces between chars - the hard way
            stringWithInputEncoding = stringWithInputEncoding.Replace("\0", String.Empty);

            //convert coding to UTF-8
            this._data = Encoding.UTF8.GetBytes(stringWithInputEncoding);
#if DEBUG
            File.WriteAllBytes(_tempFile, this._data);
            foreach (string line in File.ReadAllLines(this._tempFile))
            {
                Console.WriteLine(line);
            }
#endif
        }

        //set server connection settings
        private void CreateHttpWebRequest()
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(_url);
            request.KeepAlive = false;
            request.ContentType = "application/x-www-form-urlencoded";
            request.Method = "POST";
            request.UserAgent = "WatchDog Client - Username: " + this._userName + " - ComputerName: " + this._computerName;
            request.Credentials = CredentialCache.DefaultCredentials;
            request.ClientCertificates.Add(new X509Certificate());
            this._request = request;
        }
        #endregion

        //transfer datastreams to server
        private void SendToServer()
        {
            using (Stream stream = this._request.GetRequestStream())
            {
                stream.Write(this._fileName, 0, this._fileName.Length);
                stream.Write(this._newLine, 0, this._newLine.Length);
                stream.Write(this._data, 0, this._data.Length);
            }
        }

        //get response of server
        private bool GetServerResponse(HttpWebRequest request)
        {
            try
            {
                return CheckServerStatusCode(request);
            }
            //handle WebExceptions
            catch (WebException we)
            {
                HandleWebException(we);
            }
            catch (Exception ex)
            {
                ExceptionHandler.HandleException(ex, "Keine Verbindung zum Server möglich");
            }
            return false;
        }

        //check status code of server response
        private bool CheckServerStatusCode(HttpWebRequest request)
        {
            using (HttpWebResponse httpResponse = (HttpWebResponse)request.GetResponse())
            {
                //return true if server response with http status code 200 (OK)
                if (httpResponse.StatusCode == HttpStatusCode.OK)
                {
                    return true;
                }
            }
            return false;
        }

        #region get and log HTTP Status Code on WebException
        private void HandleWebException(WebException we)
        {
            string statusMessage = null;

            if (we.Status == WebExceptionStatus.ProtocolError)
            {
                statusMessage = GetWebExceptionProtocolError(we);
            }
            else
            {
                statusMessage = GetWebExceptionRegularError(we);
            }

            ExceptionHandler.HandleException(we, statusMessage);
        }

        private string GetWebExceptionProtocolError(WebException we)
        {
            HttpWebResponse weResponse = (HttpWebResponse)we.Response;

            if (weResponse != null && we.InnerException != null)
            {
                return "WebException - HTTP Status Code: " + (int)weResponse.StatusCode + " Grund: " + we.Message + " InnerException: " + we.InnerException.Message;
            }
            else
            {
                return "WebException - HTTP Status Code: " + (int)weResponse.StatusCode + " Grund: " + we.Message;
            }
        }

        private string GetWebExceptionRegularError(WebException we)
        {
            if (we.InnerException != null)
            {
                return "WebException - Grund: " + we.Message + " InnerException: " + we.InnerException.Message;
            }
            else
            {
                return "WebException - Grund: " + we.Message;
            }
        }
        #endregion        
    }
}