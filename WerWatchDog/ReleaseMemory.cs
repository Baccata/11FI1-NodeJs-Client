﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using NodeJsClientService;

namespace NodeJsClientService
{
    //release memory usage
    internal static class ReleaseMemory
    {
        internal static void FreeRam()
        {
            try
            {
                NativeMethods.EmptyWorkingSet(Process.GetCurrentProcess().Handle);
            }
            catch (DllNotFoundException dllEx)
            {
                ExceptionHandler.HandleException(dllEx, "psapi.dll couldn't be found");
                UseGC();
            }
            catch (Exception ex)
            {
                ExceptionHandler.HandleException(ex, "An error occured - check log file for more information");
            }
        }

        //release memory the old fashioned way
        private static void UseGC()
        {
            GC.Collect();
            GC.WaitForPendingFinalizers();
        }
    }
}