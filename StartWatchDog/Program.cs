﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace StartWatchDog
{
    internal class Program
    {
        internal static void Main()
        {
            //set new instance of ProcessStartInfo and map to XMLWatchDog
            ProcessStartInfo startInfo = SetBackgroundSettings();

            RunWatchDog(startInfo);

            ExitLauncher();
        }

        private static ProcessStartInfo SetBackgroundSettings()
        {
            ProcessStartInfo startInfo = new ProcessStartInfo("NodeJsWatchDog.exe");
            //run WatchDog in background
            startInfo.RedirectStandardOutput = true;
            startInfo.UseShellExecute = false;
            startInfo.CreateNoWindow = true;
            startInfo.WindowStyle = ProcessWindowStyle.Hidden;
            return startInfo;
        }

        //run NodeJs Client WatchDog
        private static void RunWatchDog(ProcessStartInfo startInfo)
        {
            //create new Process and map ProcessStartInfo settings
            Process process = new Process();
            process.StartInfo = startInfo;
            process.Start();
        }

        //exit program
        private static void ExitLauncher()
        {
            Environment.Exit(0);
        }
    }
}