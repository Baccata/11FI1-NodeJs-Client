﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Web.Script.Serialization;

namespace NodeJsClientService
{
    //maps values and returns serialized json string
    internal class Mapper
    {
        #region variables
        private string _appName;
        private string _appPath;
        private int _reportType;
        private string _nameOfDirectory;
        private DateTime _creationTimeUtc;
        private JavaScriptSerializer _serializer = new JavaScriptSerializer();
        private Wrapper wrapper = new Wrapper();
        #endregion

        //do mapping of values and return json string
        internal string TriggerMapping(string nameOfDirectory, string fileName)
        {
            this._nameOfDirectory = nameOfDirectory;

            this.ReadAndValidateValuesOfFile(fileName);

            //get time of creation of report.wer file
            this.GetCreationTimeOfFile(fileName);

            //get and return json string
            return GetSerializedJsonString();
        }

        //reads and sets values from file (fileName) and its creation time
        private void ReadAndValidateValuesOfFile(string fileName)
        {
            //read values from file
            this.MapValuesFromFile(this.ReadContentOfFile(fileName));

            //check values
            this.CheckIfReadOutOfFileWasSuccessful();
        }

        #region get data from file
        //return file content
        private string ReadContentOfFile(string filePath)
        {
            return Encoding.Unicode.GetString(File.ReadAllBytes(filePath));
        }

        //map data from Report.wer to class variables
        private void MapValuesFromFile(string fileContent)
        {
            foreach (string line in this.GetAllLinesFromFile(fileContent))
            {
                if (line.StartsWith(AcceptedValues.AppName.ToString()))
                {
                    this._appName = this.GetValueOfLine(line);
                }
                else if (line.StartsWith(AcceptedValues.AppPath.ToString()))
                {
                    this._appPath = this.GetValueOfLine(line);
                }
                else if (line.StartsWith(AcceptedValues.ReportType.ToString()))
                {
                    this._reportType = this.GetReportTypeNumber(line);
                }
            }
        }

        //get and return all lines from input file to string array
        private string[] GetAllLinesFromFile(string fileContent)
        {
            return fileContent.Split(new string[] { Environment.NewLine }, StringSplitOptions.None);
        }

        //returns value after equals sign
        private string GetValueOfLine(string line)
        {
            try
            {
                return line.Substring(line.LastIndexOf('=') + 1);
            }
            catch
            {
                return String.Empty;
            }
        }

        //returns value of report type number
        private int GetReportTypeNumber(string line)
        {
            int number;
            return Int32.TryParse(this.GetValueOfLine(line), out number) ? number : 6;
        }
        #endregion

        //check if appName and appPath variables are not null
        private void CheckIfReadOutOfFileWasSuccessful()
        {
            if (this._appName == null || this._appPath == null)
            {
                throw new Exception("unable to get data from file");
            }
        }

        //reads creation time of input file in utc
        private void GetCreationTimeOfFile(string file)
        {
            try
            {
                this._creationTimeUtc = File.GetCreationTimeUtc(file);
            }
            catch
            {
                this._creationTimeUtc = DateTime.MinValue;
            }
        }

        //get and return json serialized string
        private string GetSerializedJsonString()
        {
            JsonProperties mappedObject = this.GetValidJsonObject();

            //serialize and return json object
            return this._serializer.Serialize(mappedObject);
        }

        #region get model object
        //executes wrapper and returns valid json object from it
        private JsonProperties GetValidJsonObject()
        {
            this.ExecuteWrapper();

            return wrapper.GetValidJsonObject();
        }

        #region trigger wrapper for validating content
        private void ExecuteWrapper()
        {
            this.WrapReportFileContent();

            this.WrapMachineContent();

            this.WrapEnvironmentContent();

        }

        private void WrapReportFileContent()
        {
            wrapper.WrapAppName(this._appName);
            wrapper.WrapAppPath(this._appPath);
            wrapper.WrapReportType(this._reportType);
        }

        private void WrapMachineContent()
        {
            wrapper.WrapMacAddress();
            wrapper.WrapUserName();
            wrapper.WrapComputerName();
        }

        private void WrapEnvironmentContent()
        {
            wrapper.WrapNameOfDirectory(this._nameOfDirectory);
            wrapper.WrapTimestamp(this.GetUnixTimestamp(this._creationTimeUtc));
        }
        #endregion

        //returns an unix timestamp (int) based on input DateTime object
        private int GetUnixTimestamp(DateTime input)
        {
            return (int)(input.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
        }
        #endregion
    }
}