﻿namespace NodeJsClientService
{
    partial class ProjectInstaller
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.serviceProcessInstaller1 = new System.ServiceProcess.ServiceProcessInstaller();
            this.NodeJsClientWindowsService = new System.ServiceProcess.ServiceInstaller();
            // 
            // serviceProcessInstaller1
            // 
            this.serviceProcessInstaller1.Password = null;
            this.serviceProcessInstaller1.Username = null;
            // 
            // NodeJsClientWindowsService
            // 
            this.NodeJsClientWindowsService.DelayedAutoStart = true;
            this.NodeJsClientWindowsService.Description = "Watches windows error reports folder for report files and uploads it\'s content to" +
    " our server";
            this.NodeJsClientWindowsService.DisplayName = "Watches windows error reports folder for report files and uploads it\'s content to" +
    " our server";
            this.NodeJsClientWindowsService.Installers.AddRange(new System.Configuration.Install.Installer[] {
            this.serviceProcessInstaller1});
            this.NodeJsClientWindowsService.ServiceName = "Node.js Client Windows Service";
            this.NodeJsClientWindowsService.StartType = System.ServiceProcess.ServiceStartMode.Automatic;
            // 
            // ProjectInstaller
            // 
            this.Installers.AddRange(new System.Configuration.Install.Installer[] {
            this.NodeJsClientWindowsService});

        }

        #endregion

        private System.ServiceProcess.ServiceProcessInstaller serviceProcessInstaller1;
        private System.ServiceProcess.ServiceInstaller NodeJsClientWindowsService;
    }
}