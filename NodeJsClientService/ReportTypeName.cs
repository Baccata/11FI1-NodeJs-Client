﻿namespace NodeJsClientService
{
    internal enum ReportTypeName : byte
    {
        Noncritical,
        Critical,
        ApplicationCrash,
        ApplicationHang,
        Kernel,
        Invalid,
        Unknown
    }
}
