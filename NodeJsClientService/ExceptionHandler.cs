﻿using System;

namespace NodeJsClientService
{
    //handle and process exeptions
    public static class ExceptionHandler
    {
        internal static int errorCounter;

        //get exception (ex), error message and increase counter command 
        public static void HandleException(Exception ex, string errorMessage, bool increaseCounter = true)
        {
            IncreaseCounter(increaseCounter);

            if (AreArgumentsValide(ex, errorMessage))
            {
                LogExeption(ex, errorMessage);
            }
        }

        //increase errorCounter if true
        private static void IncreaseCounter(bool increaseCounter)
        {
            if (increaseCounter)
            {
                errorCounter++;
            }
        }

        //returns true, if Exception and message are nether null
        private static bool AreArgumentsValide(Exception ex, string errorMessage)
        {
            return ex != null && !String.IsNullOrWhiteSpace(errorMessage);
        }

        //log exception to log file (and console if on debug mode)
        private static void LogExeption(Exception ex, string errorMessage)
        {
            LogErrorMessage(ex, errorMessage);
#if DEBUG
            WriteErrorMessageToConsole(ex, errorMessage);
#endif
        }

        #region write error message to log file
        private static void LogErrorMessage(Exception ex, string errorMessage)
        {
            if (ex.InnerException != null)
            {
                WriteErrorMessageWithInnerException(ex, errorMessage);
            }
            else
            {
                WriteErrorMessageWithoutInnerException(ex, errorMessage);
            }
        }

        private static void WriteErrorMessageWithInnerException(Exception ex, string errorMessage)
        {
            Log.DoLogging(String.Format("Error: {0}: {1} - InnerException: {2}", errorMessage, ex, ex.InnerException));
        }

        private static void WriteErrorMessageWithoutInnerException(Exception ex, string errorMessage)
        {
            Log.DoLogging(String.Format("Error: {0}: {1}", errorMessage, ex));
        }
        #endregion

        #region write error message to console
        private static void WriteErrorMessageToConsole(Exception ex, string errorMessage)
        {
            if (ex.InnerException != null)
            {
                WriteErrorMessageWithInnerExceptionToConsole(ex, errorMessage);
            }
            else
            {
                WriteErrorMessageWithoutInnerExceptionToConsole(ex, errorMessage);
            }
        }

        private static void WriteErrorMessageWithInnerExceptionToConsole(Exception ex, string errorMessage)
        {
            Console.WriteLine("Error: {0}: {1} - InnerException: {2}", errorMessage, ex, ex.InnerException);
        }
        private static void WriteErrorMessageWithoutInnerExceptionToConsole(Exception ex, string errorMessage)
        {
            Console.WriteLine("Error: {0}: {1}", errorMessage, ex);
        }
        #endregion
    }
}