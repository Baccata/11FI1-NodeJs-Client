﻿using System.ComponentModel.DataAnnotations;

namespace NodeJsClientService
{
    public class JsonProperties
    {
        #region values from Report.wer file content
        [DataType(DataType.Text)]
        [StringLength(259)]
        public string AppName { get; set; }

        [DataType(DataType.Text)]
        [StringLength(247)]
        public string AppPath { get; set; }

        [DataType(DataType.Text)]
        [Range(0, 6)]
        public int ReportType { get; set; }

        [DataType(DataType.Text)]
        [StringLength(16)]
        public string ReportTypeName { get; set; }
        #endregion

        #region values from machine
        [DataType(DataType.Text)]
        [StringLength(12)]
        public string MacAddress { get; set; }

        [DataType(DataType.Text)]
        [StringLength(104)]
        public string UserName { get; set; }

        [DataType(DataType.Text)]
        [StringLength(15)]
        public string ComputerName { get; set; }
        #endregion

        #region values from Report.wer file attributes
        [DataType(DataType.Text)]
        [StringLength(255)]
        public string NameOfDirectory { get; set; }

        //unix timestamp in utc time
        [DataType(DataType.Text)]
        public int CreationTimestamp { get; set; }
        #endregion
    }
}