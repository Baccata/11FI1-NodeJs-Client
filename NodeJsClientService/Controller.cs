﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading;

namespace NodeJsClientService
{
    //controlls whole application
    public class Controller
    {
        #region variables
        private Mapper _mapper = new Mapper();
        private Upload _sendInfo = new Upload();
        private IDictionary<string, string> _dictWithWorkingItems = new Dictionary<string, string>();

        private Thread _mainThread;
        private Thread _monitorFolder;
        private Thread _machineOffFiles;
        private Thread _keepAnEyeAtRamUsage;
        #endregion

        //point of entry
        public void Start()
        {
            try
            {
                this.SetThreads();

                this.InitializeProgram();
            }
            catch (Exception ex)
            {
                ExceptionHandler.HandleException(ex, ex.Message);
            }
        }

        #region manage threads
        //set all threads
        private void SetThreads()
        {
            this.SetMainThread();
            this.MonitorFoldersThread();
            this.SetTriggerThread();
            this.SetRamUsageThread();
        }

        //set mainThread
        private void SetMainThread()
        {
            this._mainThread = Thread.CurrentThread;
        }

        //monitors folders thread
        private void MonitorFoldersThread()
        {
            this._monitorFolder = new Thread(new ThreadStart(this.MonitorFolders));
            this._monitorFolder.Priority = ThreadPriority.BelowNormal;
            this._monitorFolder.IsBackground = true;
            this._monitorFolder.Start();
        }

        //triggers file transfer if file has been spotted
        private void SetTriggerThread()
        {
            this._machineOffFiles = new Thread(new ThreadStart(this.IsThereAnyWorkToDo));
            this._machineOffFiles.Priority = ThreadPriority.BelowNormal;
            this._machineOffFiles.IsBackground = true;
            this._machineOffFiles.Start();
        }

        //watch out for high memory usage
        private void SetRamUsageThread()
        {
            this._keepAnEyeAtRamUsage = new Thread(new ThreadStart(this.WatchRamUsage));
            this._keepAnEyeAtRamUsage.Priority = ThreadPriority.BelowNormal;
            this._keepAnEyeAtRamUsage.IsBackground = true;
            this._keepAnEyeAtRamUsage.Start();
        }
        #endregion

        //main thread
        private void InitializeProgram()
        {
            //nothing - the main thread is boring as hell

            //don't close main thread
            while (true)
            {
                Thread.Sleep(1);
            }
        }

        #region get files from folders and add those to dictionary
        //monitors folders
        private void MonitorFolders()
        {
            while (true)
            {
                this.CheckForFilesInFolders();
                Thread.Sleep(500);
            }
        }

        //check if there are already existing subfolders in monitored directory
        private void CheckForFilesInFolders()
        {
            foreach (string subfolder in this.GetSubfolders())
            {
                this.CheckIfDirectoryExists(subfolder);
            }
        }

        //returns a list with all subfolders of watcher directories
        private IList<string> GetSubfolders()
        {
            List<string> subfolders = this.GetLocalAppDataSubfolders();
            subfolders.AddRange(this.GetProgramDataSubfolders());

            return subfolders;
        }

        //returns a list with local appdata subfolders
        private List<string> GetLocalAppDataSubfolders()
        {
            return Directory.GetDirectories(ProgramUtils.watchDogLocalAppData).ToList();
        }

        //returns a list with programdata subfolders
        private IList<string> GetProgramDataSubfolders()
        {
            return Directory.GetDirectories(ProgramUtils.watchDogProgramData).ToList();
        }

        //check if folder is still existing
        private void CheckIfDirectoryExists(string folderPath)
        {
            if (Directory.Exists(folderPath))
            {
                this.CheckFlagOfFile(folderPath);
            }
        }

        //read flag info of changed item (file/folder)
        private void CheckFlagOfFile(string folderPath)
        {
            if (this.GetFlagofFile(folderPath))
            {
                this.SetVariableNames(folderPath);
            }
        }

        //returns true if item is a folder and also compressed (blue written in MS explorer)
        private bool GetFlagofFile(string folderPath)
        {
            //get file attributes
            FileAttributes attributes = File.GetAttributes(folderPath);

            return (attributes.HasFlag(FileAttributes.Directory) && attributes.HasFlag(FileAttributes.Compressed));
        }

        //set name of directory and file path
        private void SetVariableNames(string folderPath)
        {
            this.AddItemToDict(folderPath + ProgramUtils.reportWER, Path.GetFileName(folderPath));
        }

        //add item (key = folder name string | value = filename path+name string) to dictionary
        private void AddItemToDict(string filePath, string nameOfDirectory)
        {
            if (this.AddingPossible(filePath, nameOfDirectory))
            {
                this._dictWithWorkingItems.Add(nameOfDirectory, filePath);
            }
        }

        //check if Report.wer exists and isn't already added to dictionary, returns true if so, otherwise false
        private bool AddingPossible(string filePath, string nameOfDirectory)
        {
            return File.Exists(filePath) && !this._dictWithWorkingItems.ContainsKey(nameOfDirectory);
        }
        #endregion

        //check if there are files to transfer
        private void IsThereAnyWorkToDo()
        {
            while (this._mainThread.IsAlive)
            {
                if (this._dictWithWorkingItems.Any())
                {
                    this.HandleWork();
                }
                Thread.Sleep(1);
            }
        }

        //process dictionary entries
        private void HandleWork()
        {
            for (int index = this._dictWithWorkingItems.Count - 1; index >= 0; index--)
            {
                this.StepThroughEveryKey(this.GetKeyFromDictionary(index));
            }
        }

        private KeyValuePair<string, string> GetKeyFromDictionary(int index)
        {
            return this._dictWithWorkingItems.ElementAt(index);
        }

        //handle files one by one
        private void StepThroughEveryKey(KeyValuePair<string, string> entry)
        {
            this.CheckForCorrectWork(entry.Key, entry.Value);

            this.RemoveKey(entry.Key);
        }

        //remove key from dictionary
        private void RemoveKey(string key)
        {
            this._dictWithWorkingItems.Remove(key);

            Log.DoLogging(key + ProgramUtils.processedFile);
        }

        //check for successful work
        private void CheckForCorrectWork(string key, string value)
        {
            Log.DoLogging(ProgramUtils.processingFile + key);

            try
            {
                if (!this.ProcessFile(key, value))
                {
                    this.TriggerError(key);
                }
            }
            catch (Exception ex)
            {
                this.HandleErrorsWhileWorking(ex);
            }
        }

        //trigger error
        private void TriggerError(string key)
        {
            Log.DoLogging(ProgramUtils.errorOccured + key);

            this.HandleErrorsWhileWorking(new Exception("an error occured!"));
        }

        //on error increase counter and restart method after 1 second
        private void HandleErrorsWhileWorking(Exception ex)
        {
            ExceptionHandler.errorCounter++;
            if (ExceptionHandler.errorCounter <= 5)
            {
                Thread.Sleep(1000);
                this.HandleWork();
            }
            else
            {
                ExceptionHandler.HandleException(ex, ex.Message);
                Thread.Sleep(3600000);  //wait 1 hour before next try
            }
        }

        //do something with the file
        private bool ProcessFile(string nameOfDirectory, string filePath)
        {
            //trigger transport to server
            if (this.CheckIfFileExists(nameOfDirectory, filePath))
            {
                //delete directory after successful transfer to server
                return this.DeleteDirectory(filePath);
            }
            return false;
        }

        //check if file to transfer exists at all
        private bool CheckIfFileExists(string nameOfDirectory, string filePath)
        {
            return File.Exists(filePath) ? this.GetThingsDone(nameOfDirectory, filePath) : false;
        }

        //check if file is not locked
        private bool GetThingsDone(string nameOfDirectory, string filePath)
        {
            FileInfo fileInfo = new FileInfo(filePath);

            //wait until file is completely created
            while (!this.IsFileLocked(fileInfo))
            {
                return this.TriggerTransferToServer(nameOfDirectory, filePath);
            }
            return false;
        }

        //triggers transfer to server
        private bool TriggerTransferToServer(string nameOfDirectory, string filePath)
        {
            string json = this.GetJson(nameOfDirectory, filePath);

            //trigger transport to server
            return this._sendInfo.TriggerJsonSend(json);
        }

        //returns serialized json string
        private string GetJson(string nameOfDirectory, string filePath)
        {
            return this._mapper.TriggerMapping(nameOfDirectory, filePath);
        }

        //delete folder - returns true if delete was possible
        private bool DeleteDirectory(string filePath)
        {
            try
            {
                //delete directory with all subfolders and files in it
                Directory.Delete(Path.GetDirectoryName(filePath), true);
                return true;
            }
            catch
            {
                return false;
            }
        }

        //check if file is locked (returns true) or available (false)
        private bool IsFileLocked(FileInfo file)
        {
            try
            {
                return this.TryToOpenFile(file);
            }
            //file is unavailable
            catch (IOException)
            {
                Thread.Sleep(1000);
            }
            catch { }
            return true;
        }

        //check if file is no longer used by other programs/processes
        private bool TryToOpenFile(FileInfo file)
        {
            using (FileStream stream = file.Open(FileMode.Open, FileAccess.ReadWrite, FileShare.None)) { }

            //file is not locked - hurray
            return false;
        }

        //release unused RAM every 60 seconds
        private void WatchRamUsage()
        {
            while (this._mainThread.IsAlive)
            {
                //wait 1 Minute
                Thread.Sleep(60000);

                //release memory useage
                ReleaseMemory.FreeRam();
            }
        }
    }
}