﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace NodeJsClientService
{
    //log events in txt file
    public static class Log
    {
        #region variables
        private static string _logPath = ProgramUtils.storageDirectory + @"\log\";
        private static string _logFile = logPath + "log.txt";
        private static string logPath
        {
            get { return _logPath; }
        }
        internal static string logFile
        {
            get { return _logFile; }
        }
        #endregion

        //receives logging message
        public static void DoLogging(string message)
        {
            try
            {
                ProcessMessage(message);
            }
#if DEBUG
            catch (Exception ex)
            {
                Console.WriteLine("unable to create log. Cause: " + ex.ToString());
            }
#else
            catch { }
#endif
        }

        //process input message
        private static void ProcessMessage(string message)
        {
            CheckAndCreateDirectoryIfExists();
            CheckIfLogFileExists();

            WriteMessageInLogFile(message);
        }

        //check and create directory if it not exists
        private static void CheckAndCreateDirectoryIfExists()
        {
            HelperClass.CheckDirectory(logPath);
        }

        //check and create log file if it not exists
        private static void CheckIfLogFileExists()
        {
            if (!HelperClass.CheckIfFileExists(logFile))
            {
                InitializeLogFile();
            }
        }

        //initialize writing header in log file
        private static void InitializeLogFile()
        {
            using (StreamWriter writer = File.AppendText(logFile))
            {
                WriteHeaderInLogFile(writer);
            }
        }

        //write message in log file
        private static void WriteMessageInLogFile(string message)
        {
            using (StreamWriter writer = File.AppendText(logFile))
            {
                LoggingMessage(writer, message);
            }
        }

        //write message with timestamp in log file
        private static void LoggingMessage(TextWriter txtWriter, string logMessage)
        {
            CheckForLastWriteTime(txtWriter);

            WriteMessageInLogFile(txtWriter, logMessage);
        }

        //write message with timestamp in log file
        private static void WriteMessageInLogFile(TextWriter txtWriter, string logMessage)
        {
            txtWriter.WriteLine("{0} - {1} | {2}", DateTime.Now.ToShortDateString(), DateTime.Now.ToLongTimeString(), logMessage);
        }

        //add horizontal ruler 
        private static void CheckForLastWriteTime(TextWriter txtWriter)
        {
            if (IsLastWriteTimeWithinLast3Hours())
            {
                AddHorizontalRuler(txtWriter);
            }
        }

        //returns true, if file has not been written within 3 hours
        private static bool IsLastWriteTimeWithinLast3Hours()
        {
            return File.GetLastWriteTimeUtc(logFile) < DateTime.UtcNow.AddHours(-3);
        }

        //write header in log file
        private static void WriteHeaderInLogFile(TextWriter txtWriter)
        {
            txtWriter.WriteLine(ProgramUtils.appName + " Log:");
            AddHorizontalRuler(txtWriter);
            txtWriter.WriteLine("DD.MM.YYYY - HH:MM:SS | Message");
            AddHorizontalRuler(txtWriter);
        }

        //write horizontal ruler in log file
        private static void AddHorizontalRuler(TextWriter txtWriter)
        {
            txtWriter.WriteLine("-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-");
        }
    }
}