﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace NodeJsClientService
{
    //release memory usage
    internal static class ReleaseMemory
    {
        internal static void FreeRam()
        {
            try
            {
                EmptyWorkingSet();
            }
            catch (DllNotFoundException dllEx)
            {
                ExceptionHandler.HandleException(dllEx, "psapi.dll couldn't be found");
                UseGC();
            }
            catch (Exception ex)
            {
                ExceptionHandler.HandleException(ex, "An error occured - check log file for more information");
            }
        }

        //trigger memory release
        private static void EmptyWorkingSet()
        {
            NativeMethods.EmptyWorkingSet(Process.GetCurrentProcess().Handle);
        }

        //release memory the old fashioned way
        private static void UseGC()
        {
            GC.Collect();
            GC.WaitForPendingFinalizers();
        }
    }
}