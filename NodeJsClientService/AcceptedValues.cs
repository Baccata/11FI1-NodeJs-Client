﻿namespace NodeJsClientService
{
    internal enum AcceptedValues : byte
    {
        AppName,
        AppPath,
        ReportType
    }
}