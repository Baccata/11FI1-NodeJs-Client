﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Management;
using System.Net.NetworkInformation;
using System.Text;

namespace NodeJsClientService
{
    internal static class ProgramUtils
    {
        #region constant values
        internal const string appName = "Node.js Client WatchDog";

        internal const string processingFile = "Processing following file: ";
        internal const string processedFile = " successfully transfered to server";
        internal const string errorOccured = "An error occured while processing ";
        internal const string reportWER = @"\Report.wer";
        #endregion

        //server url for receiving json data
        internal static readonly Uri url = new Uri(@"https://push.tweakl.com:443/");

        private static string localAppData = GetLocalAppDataFolder();
        private static string programData = GetProgramDataFolder();
        private static string werReportPath = @"\Microsoft\Windows\WER\ReportArchive\";

        internal static string storageDirectory = localAppData + @"\NodeJs Client WatchDog";
        internal static string watchDogLocalAppData = localAppData + werReportPath;
        internal static string watchDogProgramData = programData + werReportPath;

        internal static string CurrentUserName { get; private set; }

        private static string _currentMacAddress = GetMacAddress();
        internal static string CurrentMacAddress
        {
            get { return _currentMacAddress; }
        }

        #region get local app data folder
        //returns the path to local app data folder - %LocalAppData%
        private static string GetLocalAppDataFolder()
        {
            CurrentUserName = GetCurrentUserName();
            return GetActualUserDirectory(GetAllUserDirectories()) + @"\AppData\Local";
        }

        //returns the path to the longest user directory from currently logged on user
        private static string GetActualUserDirectory(string[] array)
        {
            return array
                .Where(x => x.Contains(CurrentUserName))
                .OrderByDescending(x => x.Normalize())  //alternate: get rid of this line and use LastOrDefault
                .FirstOrDefault();
        }

        //returns an array with all user directories
        private static string[] GetAllUserDirectories()
        {
            return Directory.GetDirectories(GetUserPath());
        }

        //returns the path to the user directories
        private static string GetUserPath()
        {
            return GetWindowsInstallDriveLetter() + @"Users\";
        }

        //returns the drive letter with contains the windows installation (e.g. C:\ )
        private static string GetWindowsInstallDriveLetter()
        {
            return Path.GetPathRoot(Environment.GetFolderPath(Environment.SpecialFolder.System));
        }
        #endregion

        //get program data folder - %ProgramData%
        private static string GetProgramDataFolder()
        {
            return Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData);
        }

        //returns mac address of current network device as a string without colons or hyphens
        private static string GetMacAddress()
        {
            return NetworkInterface.GetAllNetworkInterfaces()
                .Where(nic => nic.NetworkInterfaceType == NetworkInterfaceType.Ethernet && nic.OperationalStatus == OperationalStatus.Up)
                .Select(nic => nic.GetPhysicalAddress().ToString())
                .FirstOrDefault();
        }

        #region get username
        //get username of currently logged in user
        private static string GetCurrentUserName()
        {
            ManagementObjectCollection collection = GetCollection();
            string deviceNameAndUserName = GetDeviceAndUserNameFromManagementObjectCollection(collection);
            return deviceNameAndUserName.Substring(deviceNameAndUserName.LastIndexOf('\\') + 1);
        }

        //returns pc name and username
        private static string GetDeviceAndUserNameFromManagementObjectCollection(ManagementObjectCollection collection)
        {
            return (string)collection.Cast<ManagementBaseObject>().First()["UserName"];
        }

        private static ManagementObjectCollection GetCollection()
        {
            ManagementObjectSearcher searcher = new ManagementObjectSearcher(@"SELECT UserName FROM Win32_ComputerSystem");
            return searcher.Get();
        }
        #endregion
    }
}