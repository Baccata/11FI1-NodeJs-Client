﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace NodeJsClientService
{
    internal static class HelperClass
    {
        //trigger directory creation
        internal static bool CheckDirectory(string path)
        {
            try
            {
                return CreateDirectory(path);
            }
            catch (Exception ex)
            {
                ExceptionHandler.HandleException(ex, "unable to create " + path + " directory");
                return false;
            }
        }

        //create directory on defined path
        private static bool CreateDirectory(string path)
        {
            if (!CheckIfDirectoryExists(path))
            {
                Directory.CreateDirectory(path);
            }
            return true;
        }

        //returns true if directory exists
        private static bool CheckIfDirectoryExists(string path)
        {
            return Directory.Exists(path);
        }

        //returns true if file exists
        internal static bool CheckIfFileExists(string pathToFile)
        {
            return File.Exists(pathToFile);
        }
    }
}