﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace NodeJsClientService
{
    //uploads json to server
    public class Upload
    {
        #region variables
        //holds transfer details
        private HttpWebRequest _request;

        //get name of client machine (pc)
        private readonly string _computerName = Environment.MachineName;
        //get name of client user
        private readonly string _userName = Environment.UserName;
        #endregion

        //receives json string for upload to server
        internal bool TriggerJsonSend(string json)
        {
            try
            {
                //set transfer details
                this.CreateHttpWebRequest();

                //transfer data to server
                this.SendJsonToServer(json);

                //get server response
                return this.GetServerResponse();
            }
            catch (Exception ex)
            {
                ExceptionHandler.HandleException(ex, "unable to send json to server");
                return false;
            }
        }

        #region Initialize connection
        //set server connection settings
        private void CreateHttpWebRequest()
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(ProgramUtils.url);
            request.KeepAlive = false;
            request.ContentType = "application/x-www-form-urlencoded";
            request.Method = WebRequestMethods.Http.Post;
            request.UserAgent = this.GetUserAgent();
            request.Credentials = CredentialCache.DefaultCredentials;
            request.ClientCertificates.Add(new X509Certificate());
            this._request = request;
        }

        //returns current user agent
        private string GetUserAgent()
        {
            return String.Format("WatchDog Client - Username: {0} - ComputerName: {1}", this._userName, this._computerName);
        }
        #endregion

        #region send json to server
        //transfer json datastream to server
        private void SendJsonToServer(string json)
        {
            byte[] jsonBytes = this.GetJsonByteArray(json);

            //and liftoff
            using (Stream stream = this._request.GetRequestStream())
            {
                stream.Write(jsonBytes, 0, jsonBytes.Length);
            }
        }

        //returns byte array based on input string
        private byte[] GetJsonByteArray(string json)
        {
            return Encoding.UTF8.GetBytes(json);
        }
        #endregion

        #region get server response
        //get response of server
        private bool GetServerResponse()
        {
            try
            {
                return this.CheckServerStatusCode();
            }
            //handle web exceptions
            catch (WebException we)
            {
                this.HandleWebException(we);
            }
            //handle non web exceptions
            catch (Exception ex)
            {
                ExceptionHandler.HandleException(ex, "Unable to connect to server");
            }
            return false;
        }

        //check status code of server response -> return true if correct status code, otherwise false
        private bool CheckServerStatusCode()
        {
            using (HttpWebResponse response = (HttpWebResponse)this._request.GetResponse())
            {
                //return true if server response with http status code 204 (No Content) => everything went fine
                return response.StatusCode == HttpStatusCode.NoContent ? true : false;
            }
        }

        #region get and log HTTP status code if WebException occurs
        private void HandleWebException(WebException we)
        {
            if (we.Status == WebExceptionStatus.ProtocolError)
            {
                ExceptionHandler.HandleException(we, this.GetWebExceptionProtocolError(we));
            }
            else
            {
                ExceptionHandler.HandleException(we, this.GetWebExceptionRegularError(we));
            }
        }

        #region web exception protocol error message
        private string GetWebExceptionProtocolError(WebException we)
        {
            HttpWebResponse webResponse = (HttpWebResponse)we.Response;

            if (webResponse != null && we.InnerException != null)
            {
                return this.GetWebExceptionProtocolErrorWithInnerException(webResponse, we);
            }
            else
            {
                return this.GetWebExceptionProtocolErrorWithoutInnerException(webResponse, we);
            }
        }

        private string GetWebExceptionProtocolErrorWithInnerException(HttpWebResponse webResponse, WebException we)
        {
            return String.Format("WebException - HTTP status code: {0} reason: {1} InnerException: {2}", webResponse.StatusCode, we.Message, we.InnerException.Message);
        }

        private string GetWebExceptionProtocolErrorWithoutInnerException(HttpWebResponse webResponse, WebException we)
        {
            return String.Format("WebException - HTTP status code: {0} reason: {1}", webResponse.StatusCode, we.Message);
        }
        #endregion

        #region regular web exception error message
        private string GetWebExceptionRegularError(WebException we)
        {
            if (we.InnerException != null)
            {
                return this.GetRegularWebExceptionErrorWithInnerException(we);
            }
            else
            {
                return this.GetRegularWebExceptionErrorWithoutInnerException(we);
            }
        }

        private string GetRegularWebExceptionErrorWithInnerException(WebException we)
        {
            return String.Format("WebException - reason: {0} InnerException: {1}", we.Message, we.InnerException.Message);
        }
        private string GetRegularWebExceptionErrorWithoutInnerException(WebException we)
        {
            return String.Format("WebException - reason: {0}", we.Message);
        }
        #endregion
        #endregion
        #endregion
    }
}