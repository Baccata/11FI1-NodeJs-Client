﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;

namespace NodeJsClientService
{
    //checks every value of json object for valid content
    internal class Validator
    {
        internal bool ValidateAppName(string appName)
        {
            return this.ValidateString(appName, 259);
            //    return this.CheckLengthOfString(appName, 259) && this.CheckForHtmlCode(appName) ? true : false;
        }

        internal bool ValidateAppPath(string appPath)
        {
            return this.CheckLengthOfString(appPath, 247) && this.CheckPathWrapper(appPath) ? true : false;
        }

        internal bool ValidateReportType(int reportType)
        {
            return reportType >= 0 && reportType <= 6;
            //       return this.CheckIfIntegerIsWithinRange(reportType, 0, 6);
        }

        /// <summary>
        /// check if mac address only contains digits and chars between A and F
        /// </summary>
        /// <param name="macAddress">mac address to validate - make sure mac address is trimmed and contains neither colons nor hyphens</param>
        /// <returns>true if check was successfully - otherwise false</returns>
        internal bool ValidateMacAddress(string macAddress)
        {
            return Regex.IsMatch(macAddress, @"^[a-fA-F0-9]{12}$");
        }

        internal bool ValidateUserName(string userName)
        {
            return this.ValidateString(userName, 104);
            //return this.CheckLengthOfString(userName, 104) && this.CheckForHtmlCode(userName) ? true : false;
        }

        internal bool ValidateComputerName(string computerName)
        {
            return this.ValidateString(computerName, 15);
            //return this.CheckLengthOfString(computerName, 15) && this.CheckForHtmlCode(computerName) ? true : false;
        }

        internal bool ValidateNameOfDirectory(string nameOfDirectory)
        {
            return this.ValidateString(nameOfDirectory, 247);
            //return this.CheckLengthOfString(nameOfDirectory, 247) && this.CheckForHtmlCode(nameOfDirectory) ? true : false;
        }

        //check if timestamp is between 1th January 2000 and maximum value of integer
        internal bool ValidateTimestamp(int timestamp)
        {
            //946684800 == 1th January 2000
            //return this.CheckIfIntegerIsWithinRange(timestamp, 946684800, Int32.MaxValue);
            return timestamp >= 946684800 && timestamp <= Int32.MaxValue;
        }

        private bool CheckForEmptyString(string input)
        {
            return String.IsNullOrWhiteSpace(input);
        }

        //validate input string for maximum char length and illegal chars
        private bool ValidateString(string input, int maxLength)
        {
            return this.CheckLengthOfString(input, maxLength) && this.CheckForHtmlCode(input) ? true : false;
        }

        private bool CheckLengthOfString(string input, int maxLength)
        {
            return this.GetLengthOfString(input) <= maxLength ? true : false;
        }

        private int GetLengthOfString(string input)
        {
            return input.Length;
        }

        #region check input for correct path
        private bool CheckPathWrapper(string appPath)
        {
            try
            {
                return this.CheckPath(appPath);
            }
            catch
            {
                return false;
            }
        }

        //get path depending on input path and compare these two strings
        private bool CheckPath(string appPath)
        {
            return appPath == Path.GetFullPath(appPath) ? true : false;
        }
        #endregion

        [Obsolete]
        private bool CheckIfIntegerIsWithinRange(int valueToCheck, int minimumValue, int maximumValue)
        {
            return valueToCheck >= minimumValue && valueToCheck <= maximumValue;
        }

        #region methods to check if string contains html code to prevent Cross-site-Scripting (XSS)
        private bool CheckForHtmlCode(string input)
        {
            return this.IsStringContainingHtmlCode(input) && this.ContainsHTML(input) ? false : true;
        }

        private bool IsStringContainingHtmlCode(string input)
        {
            return input != HttpUtility.HtmlEncode(input);
        }

        private bool ContainsHTML(string input)
        {
            return Regex.IsMatch(input, "<(.|\n)*?>");
        }
        #endregion
    }
}