﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading;

namespace NodeJsClientService
{
    //todo: check if WatchDog is still running -> if not restart it

    //controls service | starts and stopps service | triggers WatchDog
    public partial class Scheduler : ServiceBase
    {
        private Controller _controller = new Controller();
        private ManualResetEvent _shutdownEvent = new ManualResetEvent(false);

        internal Scheduler()
        {
            InitializeComponent();
        }

        //triggers when service starts
        protected override void OnStart(string[] args)
        {
            // used for debugging mode -> attaching to service
            // Thread.Sleep(20000);
            Thread triggerWatchDog = new Thread(new ThreadStart(this.RunWatchDog));
            triggerWatchDog.Priority = ThreadPriority.BelowNormal;
            triggerWatchDog.IsBackground = true;
            triggerWatchDog.Start();

            Log.DoLogging("Node.js service started");
        }

        //Start WatchDog
        private void RunWatchDog()
        {
            this._controller.Start();
        }

        //triggers when service stops
        protected override void OnStop()
        {
            this._shutdownEvent.Close();

            Log.DoLogging("Node.js service stopped");
        }
    }
}