﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.ServiceProcess;

namespace NodeJsClientService
{
    internal static class Program
    {
        internal static void Main(string[] args)
        {
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[]
            {
                new Scheduler()
            };

            ServiceBase.Run(ServicesToRun);
        }
    }
}