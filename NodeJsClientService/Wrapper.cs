﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NodeJsClientService
{
    //wraps json object based on validator (Validator.cs) results
    internal class Wrapper
    {
        #region variables
        private Validator validator = new Validator();

        private string appName;
        private string appPath;
        private int reportType;
        private ReportTypeName reportTypeName;
        private string macAddress;
        private string userName;
        private string computerName;
        private string nameOfDirectory;
        private int creationTimestamp;
        #endregion

        #region wrap json object values if valid otherwise with error value
        internal void WrapAppName(string appName)
        {
            this.appName = validator.ValidateAppName(appName) ? appName : ErrorStrings.Invalid_AppName.ToString();
        }

        internal void WrapAppPath(string appPath)
        {
            this.appPath = validator.ValidateAppPath(appPath) ? appPath : ErrorStrings.Invalid_AppPath.ToString();
        }

        internal void WrapReportType(int reportType)
        {
            this.reportType = validator.ValidateReportType(reportType) ? reportType : 6;

            this.WrapReportTypeName();
        }

        private void WrapReportTypeName()
        {
            this.reportTypeName = (ReportTypeName)this.reportType;
        }

        internal void WrapMacAddress()
        {
            this.macAddress = validator.ValidateMacAddress(ProgramUtils.CurrentMacAddress) ? ProgramUtils.CurrentMacAddress : ErrorStrings.Invalid_MacAddress.ToString();
        }

        internal void WrapUserName()
        {
            this.userName = validator.ValidateUserName(ProgramUtils.CurrentUserName) ? ProgramUtils.CurrentUserName : ErrorStrings.Invalid_UserName.ToString();
        }

        internal void WrapComputerName()
        {
            this.computerName = validator.ValidateComputerName(Environment.MachineName) ? Environment.MachineName : ErrorStrings.Invalid_ComputerName.ToString();
        }

        internal void WrapNameOfDirectory(string nameOfDirectory)
        {
            this.nameOfDirectory = validator.ValidateNameOfDirectory(nameOfDirectory) ? nameOfDirectory : ErrorStrings.Invalid_NameOfDirectory.ToString();
        }

        //sets timestamp to -1 if timestamp is invalid
        internal void WrapTimestamp(int creationTimestamp)
        {
            this.creationTimestamp = validator.ValidateTimestamp(creationTimestamp) ? creationTimestamp : -1;
        }
        #endregion

        //create and return checked json object
        internal JsonProperties GetValidJsonObject()
        {
            return new JsonProperties
            {
                AppName = this.appName,
                AppPath = this.appPath,
                ReportType = this.reportType,
                ReportTypeName = this.reportTypeName.ToString(),
                MacAddress = this.macAddress,
                UserName = this.userName,
                ComputerName = this.computerName,
                NameOfDirectory = this.nameOfDirectory,
                CreationTimestamp = this.creationTimestamp
            };
        }
    }
}