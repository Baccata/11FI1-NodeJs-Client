﻿namespace NodeJsClientService
{
    internal enum ErrorStrings : byte
    {
        Invalid_AppName,
        Invalid_AppPath,
        Invalid_MacAddress,
        Invalid_UserName,
        Invalid_ComputerName,
        Invalid_NameOfDirectory
    }
}